import plus from "./../icons/plus.svg";
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div className="bg-home bg-cover h-screen ">
      <div className="bg-gradient-to-t from-slate-900 to-transparent h-screen m-auto">
        <div className="flex flex-col justify-center gap-5 items-center h-screen">
          <h1 className="text-white font-poppins font-bold text-3xl">
            WELCOME TO THE GAME!
          </h1>
          <Link to="/tambah">
            <img src={plus} alt="plus" />
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Home;
