module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      backgroundImage: {
        home: "url('/src/img/games.jpg')",
        form: "url('/src/img/form.jpg')",
        card: "url('/src/img/card.jpg')",
      },
      colors: {
        warna: "#0f1128",
      },
      fontFamily: {
        poppins: ["Poppins"],
      },
    },
  },
  plugins: [],
};
