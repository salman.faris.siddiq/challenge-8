import { Route, Routes } from "react-router-dom";
import "./App.css";
import Home from "./pages/Home";
import Tambah from "./pages/Tambah";

function App() {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/tambah" element={<Tambah />} />
      </Routes>
    </div>
  );
}

export default App;
