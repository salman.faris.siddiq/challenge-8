import { useFormik } from "formik";
import { useState } from "react";
import Cardplayers from "../components/Cardplayer";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import * as yup from "yup";

const validationschema = yup.object({
  username: yup.string().required("*wajib diisi"),
  email: yup.string().email("*gunakan format email"),
  experience: yup.number().required().positive().integer(),
  lvl: yup.number().required().positive().integer(),
});

export default function Tambah() {
  const [isEditing, setIsEditing] = useState(false);
  const [isSearch, setisSearch] = useState("");
  const [isbuttonsearch, setisbuttonsearch] = useState(false);
  const [currentPlayers, setcurrentPlayers] = useState([]);
  const [iseditValue, setiseditValue] = useState("");
  const [iseditUsername, setiseditUsername] = useState("");
  const [iseditEmail, setiseditEmail] = useState("");
  const [iseditExperience, setiseditExperience] = useState("");
  const [iseditLvl, setiseditLvl] = useState("");

  const formik = useFormik({
    initialValues: {
      username: "",
      email: "",
      experience: "",
      lvl: "",
    },
    onSubmit: (values, onPropsSubmit) => {
      onPropsSubmit.resetForm();

      const newTodo = {
        id: Math.floor(Math.random() * 100),
        username: values.username,
        email: values.email,
        experience: values.experience,
        lvl: values.lvl,
      };
      setcurrentPlayers([...currentPlayers, newTodo]);
      setisbuttonsearch(true);
    },
    validationSchema: validationschema,
  });
  const handleEdit = (index) => {
    setIsEditing(true);
  };

  const handleDelete = (index) => {
    setcurrentPlayers(
      currentPlayers.filter((item, currentIndex) => currentIndex !== index)
    );
  };

  const handleUpdate = (id) => {
    const updatedTodo = [...currentPlayers].map((item) => {
      if (item.id === id) {
        item.username = iseditUsername;
        item.email = iseditEmail;
        // item.experience = iseditValue.experience;
        // item.lvl = iseditValue.lvl;
      }
      return item;
    });
    setcurrentPlayers(updatedTodo);
    setIsEditing(false);
    setiseditValue("");
  };
  return (
    <div className="bg-warna">
      <Navbar />
      <div className="h-auto flex flex-col justify-center items-center  font-poppins">
        {isbuttonsearch && (
          <div className="flex flex-col justify-center items-center w-72 h-10 mb-10 text-white gap-3">
            <h1>Search...</h1>
            <input
              type="text"
              name="username"
              placeholder="username / email"
              className="bg-transparent px-3 py-1 border-b-2 text-white focus:outline-none"
              onChange={(event) => {
                setisSearch(event.target.value);
              }}
            />
          </div>
        )}

        <div className="bg-form bg-cover w-72  rounded-lg mb-10 h-4/6">
          <div className="h-full flex flex-col gap-10 py-9 justify-center items-center">
            <h1 className="text-white font-bold">Register</h1>
            <form
              className="flex flex-col gap-10 px-10"
              onSubmit={formik.handleSubmit}
            >
              <div>
                <input
                  type="text"
                  name="username"
                  placeholder="username"
                  className="bg-transparent px-3 py-1 border-b-2 text-white focus:outline-none"
                  onChange={formik.handleChange}
                  value={formik.values.username}
                  {...formik.getFieldProps("username")}
                />
                {formik.touched.username && formik.errors.username ? (
                  <div className="text-red-600 text-sm">
                    {formik.errors.username}
                  </div>
                ) : null}
              </div>
              <div>
                <input
                  type="email"
                  name="email"
                  placeholder="email"
                  className="bg-transparent px-3 py-1 border-b-2 text-white focus:outline-none"
                  onChange={formik.handleChange}
                  value={formik.values.email}
                  {...formik.getFieldProps("email")}
                />
                {formik.touched.email && formik.errors.email ? (
                  <div className="text-red-600 text-sm">
                    {formik.errors.email}
                  </div>
                ) : null}
              </div>
              <input
                type="number"
                name="experience"
                placeholder="experience"
                className="bg-transparent px-3 py-1 border-b-2 text-white focus:outline-none"
                onChange={formik.handleChange}
                value={formik.values.experience}
                {...formik.getFieldProps("experience")}
              />
              {formik.touched.experience && formik.errors.experience ? (
                <div className="text-red-600 text-sm">
                  {formik.errors.experience}
                </div>
              ) : null}
              <input
                type="number"
                name="lvl"
                placeholder="lvl"
                className="bg-transparent px-3 py-1 border-b-2 text-white focus:outline-none"
                onChange={formik.handleChange}
                value={formik.values.lvl}
                {...formik.getFieldProps("lvl")}
              />
              {formik.touched.lvl && formik.errors.lvl ? (
                <div className="text-red-600 text-sm">{formik.errors.lvl}</div>
              ) : null}

              <button
                className="px-2 py-1 text-white bg-pink-700 w-20 mx-auto rounded-lg"
                type="submit"
              >
                Submit
              </button>
            </form>
          </div>
        </div>
        <div className="flex flex-row justify-start items-start py-10 px-10 w-full">
          <Cardplayers
            players={currentPlayers}
            handleEdit={handleEdit}
            edit={isEditing}
            search={isSearch}
            handleDelete={handleDelete}
            handleUpdate={handleUpdate}
            setiseditValue={setiseditValue}
            iseditValue={iseditValue}
            isEditing={isEditing}
            setIsEditing={setIsEditing}
            iseditUsername={iseditUsername}
            setiseditUsername={setiseditUsername}
            iseditEmail={iseditEmail}
            setiseditEmail={setiseditEmail}
            iseditExperience={iseditExperience}
            setiseditExperience={setiseditExperience}
            iseditLvl={iseditLvl}
            setiseditLvl={setiseditLvl}
          />
        </div>
        <div className="mt-24 py-5">
          <Footer />
        </div>
      </div>
    </div>
  );
}
