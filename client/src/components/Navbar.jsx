import games from "./../icons/games.svg";

const Navbar = () => {
  return (
    <div>
      <div className="flex p-5 text-white font-poppins gap-2">
        <img src={games} alt="games" />
        <h1>GAMES</h1>
      </div>
    </div>
  );
};

export default Navbar;
