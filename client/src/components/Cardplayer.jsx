import players from "./../icons/player.svg";
import experience from "./../icons/clock.svg";
import email from "./../icons/email.svg";
import level from "./../icons/level.svg";

const Cardplayers = (props) => {
  return (
    <div className="flex flex-wrap gap-10">
      {props.players
        .filter((value) => {
          if (props.search === "") {
            return value;
          } else if (
            value.username.toLowerCase().includes(props.search.toLowerCase()) ||
            value.email.toLowerCase().includes(props.search.toLowerCase())
          ) {
            return value;
          }
        })
        .map((item, index) => {
          return (
            <div
              className="text-white font-poppins bg-card bg-cover p-5 h-60 w-80 flex flex-col gap-4 justify-end
           items-start rounded-lg"
            >
              <div className="flex flex-row gap-5">
                {props.isEditing === item.id ? (
                  <button
                    className="px-5 py-1 w-max bg-blue-600 rounded-lg"
                    onClick={() => props.handleUpdate(item.id)}
                  >
                    Update
                  </button>
                ) : (
                  <button
                    className="px-5 py-1 w-max bg-blue-600 rounded-lg"
                    onClick={() => props.setIsEditing(item.id)}
                  >
                    Edit
                  </button>
                )}

                <button
                  className="px-5 py-1 w-max bg-pink-700 rounded-lg"
                  onClick={() => props.handleDelete(index)}
                >
                  Delete
                </button>
              </div>
              <div className="flex gap-2">
                <img src={players} alt="players" />
                {props.isEditing === item.id ? (
                  <input
                    className="bg-transparent px-3 py-1 border-b-2 text-white focus:outline-none"
                    name="username"
                    onChange={(event) => {
                      props.setiseditUsername(event.target.value);
                    }}
                    value={props.iseditUsername}
                  />
                ) : (
                  <div>{item.username}</div>
                )}
              </div>
              <div className="flex gap-2">
                <img src={email} alt="email" />
                {props.isEditing === item.id ? (
                  <input
                    className="bg-transparent px-3 py-1 border-b-2 text-white focus:outline-none"
                    name="email"
                    onChange={(event) => {
                      props.setiseditEmail(event.target.value);
                    }}
                    value={props.iseditEmail}
                  />
                ) : (
                  <div>{item.email}</div>
                )}
              </div>
              <div className="flex gap-4">
                <div className="flex gap-2">
                  <img src={experience} alt="exp" />
                  {props.isEditing === item.id ? (
                    <input
                      className="bg-transparent px-3 py-1 border-b-2 text-white focus:outline-none w-14"
                      name="experience"
                      onChange={(event) => {
                        props.setiseditExperience(event.target.value);
                      }}
                      value={props.iseditExperience}
                    />
                  ) : (
                    <div>{item.experience}</div>
                  )}
                </div>
                <div className="flex gap-2">
                  <img src={level} alt="lvl" />
                  {props.isEditing === item.id ? (
                    <input
                      className="bg-transparent px-3 py-1 border-b-2 text-white focus:outline-none w-14"
                      name="lvl"
                      onChange={(event) => {
                        props.setiseditLvl(event.target.value);
                      }}
                      value={props.iseditLvl}
                    />
                  ) : (
                    <div>{item.lvl}</div>
                  )}
                </div>
              </div>
            </div>
          );
        })}
    </div>
  );
};

export default Cardplayers;
